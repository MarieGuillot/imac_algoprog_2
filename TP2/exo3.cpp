#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	for (int j=1; j<toSort.size(); j++) {

	bool tri = true;
		for (int i=0; i<toSort.size()-j; i++) {
			if (toSort[i]>toSort[i+1]) {
				toSort.swap(i, i+1);
				tri=false;
			}
		}
		if (tri) {
				break;
			}
	}
	
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
