#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>
#include <iostream>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    int i = heapSize;
    this->set(i, value);

    while(i>0 && this->get(i)>this->get((i-1)/2)) {
        this->swap(i, ((i-1)/2));
        i = (i-1)/2;
    }
    
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int i_max = nodeIndex;
    int largest;

    int filsGauche = -1;
    if (this->leftChild(i_max)<=heapSize-1) {
        filsGauche = this->get(this->leftChild(i_max));
    }
    int filsDroit = -1;
    if (this->rightChild(i_max)<=heapSize-1) {
        filsDroit = this->get(this->rightChild(i_max));
    }
    
    if (this->get(i_max)<filsGauche) {
        if (filsDroit<filsGauche) {
            largest = this->leftChild(i_max);
        }
        else {
            largest = this->rightChild(i_max);
        }

    } else if ((this->get(i_max))<filsDroit) {
        largest = this->rightChild(i_max);
    } else {
         largest = i_max;
    }
   
    if (largest != i_max) {
        this->swap(i_max, largest);
        heapify(heapSize, largest);
    }

}

void Heap::buildHeap(Array& numbers)
{
    int taille = numbers.size();
    for (int i=0; i<taille; i++) {
    this->insertHeapNode(i, numbers[i]);
    }
    
}

void Heap::heapSort()
{
for (int i=this->size()-1; i>=0; i--) {
    this->swap(0, i);
    this->heapify(i, 0);
}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
