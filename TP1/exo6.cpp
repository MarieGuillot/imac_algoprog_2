#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int taille;
    int capacite;
};


void initialise(Liste* liste) // Null !!!
{
    liste->premier = NULL;
    /*
liste->premier = (Noeud*) malloc (sizeof(Noeud));
if (!liste->premier) {
    cout << "Probleme d'allocation";
}
liste->premier->donnee = NULL;
liste->premier->suivant = NULL;*/
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == NULL) {
        return true;
    } else {
       return false; 
    }
    
}

void ajoute(Liste* liste, int valeur)
{

    if (liste->premier == NULL) {
        liste->premier = (Noeud*) malloc (sizeof(Noeud));
         if (!liste->premier) {
            cout << "Probleme d'allocation";
        }
        liste->premier->donnee = valeur;
        liste->premier->suivant= NULL;
    }
    else {
        Noeud* curseur = liste->premier;
        while (curseur->suivant!= NULL) {
            curseur = curseur->suivant;
        }
        curseur->suivant = (Noeud*) malloc (sizeof(Noeud));
        if (!curseur->suivant) {
            cout << "Probleme d'allocation";
        }
        curseur->suivant->donnee = valeur;
        curseur->suivant->suivant= NULL;
    }

}


void affiche(const Liste* liste)
{
Noeud* curseur = liste->premier;
while (curseur != NULL) {
cout << curseur->donnee << endl;
curseur = curseur->suivant;
}
}

int recupere(const Liste* liste, int n) 
{
    Noeud* curseur = liste->premier;
 for (int i=0; i<n; i++) {
     curseur = curseur->suivant;
 }
    if (curseur->donnee) {
        return curseur->donnee;
    }
    else {
        return 0;
    }
    
}

int cherche(const Liste* liste, int valeur)
{
    int index = 0; 
    Noeud* curseur = liste->premier;
    while (curseur != NULL) {
        if (curseur->donnee == valeur) {
            return index;
        }
        curseur = curseur->suivant;
        index++; 
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
Noeud* curseur = liste->premier;
 for (int i=0; i<n; i++) {
     curseur = curseur->suivant;
 }
    if (curseur->donnee) {
        curseur->donnee=valeur;
    }
    else {
        cout << "pas de n-eme entier a remplacer" << endl;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->taille < tableau->capacite) {
        tableau->donnees[tableau->taille]=valeur;
        tableau->taille++;
    }
    else{
        tableau->capacite++;
        tableau->donnees=(int*)realloc(tableau->donnees, sizeof(int)*(tableau->capacite));
        tableau->donnees[tableau->taille]=valeur;
        tableau->taille++;
    }


}


void initialise(DynaTableau* tableau, int capacite)
{
 tableau->donnees=(int*)malloc(sizeof(int)*capacite);
 tableau->taille=0;
 tableau->capacite=capacite;
}

bool est_vide(const DynaTableau* tableau)
{
    if (tableau->taille == 0) {
        return true;
    } else {
        return false;
    }
    
}

void affiche(const DynaTableau* tableau)
{
for (int i=0; i<tableau->taille; i++){
    cout << tableau->donnees[i] << endl;
}
}

int recupere(const DynaTableau* tableau, int n)
{
    if (tableau->taille<n) {
        cout << "L'element n'est pas dans le tableau." << endl;
        return 0;
    } else {
         return tableau->donnees[n];
    }
   
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i=0; i<tableau->taille; i++) {
        if (tableau->donnees[i]==valeur) {
            return i;
        }
    } 

        cout << valeur << " n'est pas dans le tableau." << endl;
          return -1;
    
  
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (tableau->taille<n) {
        cout << "Pas de n-ieme valeur a remplacer." << endl;
    } else {
         tableau->donnees[n]=valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (est_vide(liste)) {
        cout << "Liste vide." << endl;
        return 0;
    } else {
        int valeur = liste->premier->donnee;
    liste->premier=liste->premier->suivant;
    return valeur;
    }
    
}

void pousse_pile(DynaTableau* liste, int valeur) //void pousse_pile(Liste* liste, int valeur)
{
ajoute(liste, valeur);
}

int retire_pile(DynaTableau* liste) //int retire_pile(Liste* liste)
{
    int valeur = liste->donnees[liste->taille-1];
    liste->donnees[liste->taille-1]=0;
    liste->taille--;
    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste elle n'est pas vide au début" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste elle reste vide" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    //Liste pile; 
     DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile, 0);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i); 
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0) 
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là... (liste)" << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là... (pile)" << std::endl;
    }

    return 0;
}
