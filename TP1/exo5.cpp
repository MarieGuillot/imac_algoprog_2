#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){

  if (n>0) {
          if ((z.x*z.x+z.y*z.y) > 4) {
          return 0; 
    }
    else {
        float x = z.x;
        float y = z.y;
        float a = point.x;
        float b = point.y;
        Point suivant;
        suivant.x = x*x - y*y + a;
        suivant.y = 2*x*y+b;
        isMandelbrot(suivant, n-1, point);
    }
    }
    else {
        return 1;
    }
    
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



